# LinksClient::LinksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](LinksApi.md#create) | **POST** /v1 | 
[**show**](LinksApi.md#show) | **GET** /v1/{code} | 
[**stats**](LinksApi.md#stats) | **POST** /v1/stats | 
[**stats_by_date**](LinksApi.md#stats_by_date) | **POST** /v1/stats_by_date | 
[**visits_sum**](LinksApi.md#visits_sum) | **POST** /v1/visits_sum | 


# **create**
> LinksCreateResponse create(body)



### Example
```ruby
# load the gem
require 'links-client'

api_instance = LinksClient::LinksApi.new

body = LinksClient::LinksLinkData.new # LinksLinkData | 


begin
  result = api_instance.create(body)
  p result
rescue LinksClient::ApiError => e
  puts "Exception when calling LinksApi->create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LinksLinkData**](LinksLinkData.md)|  | 

### Return type

[**LinksCreateResponse**](LinksCreateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **show**
> LinksLinkData show(code)



### Example
```ruby
# load the gem
require 'links-client'

api_instance = LinksClient::LinksApi.new

code = "code_example" # String | 


begin
  result = api_instance.show(code)
  p result
rescue LinksClient::ApiError => e
  puts "Exception when calling LinksApi->show: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**|  | 

### Return type

[**LinksLinkData**](LinksLinkData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **stats**
> LinksStatsResponse stats(body)



### Example
```ruby
# load the gem
require 'links-client'

api_instance = LinksClient::LinksApi.new

body = LinksClient::LinksStatsRequest.new # LinksStatsRequest | 


begin
  result = api_instance.stats(body)
  p result
rescue LinksClient::ApiError => e
  puts "Exception when calling LinksApi->stats: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LinksStatsRequest**](LinksStatsRequest.md)|  | 

### Return type

[**LinksStatsResponse**](LinksStatsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **stats_by_date**
> LinksStatsResponse stats_by_date(body)



### Example
```ruby
# load the gem
require 'links-client'

api_instance = LinksClient::LinksApi.new

body = LinksClient::LinksStatsByDateRequest.new # LinksStatsByDateRequest | 


begin
  result = api_instance.stats_by_date(body)
  p result
rescue LinksClient::ApiError => e
  puts "Exception when calling LinksApi->stats_by_date: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LinksStatsByDateRequest**](LinksStatsByDateRequest.md)|  | 

### Return type

[**LinksStatsResponse**](LinksStatsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **visits_sum**
> LinksVisitsSumResponse visits_sum(body)



### Example
```ruby
# load the gem
require 'links-client'

api_instance = LinksClient::LinksApi.new

body = LinksClient::LinksVisitsSumRequest.new # LinksVisitsSumRequest | 


begin
  result = api_instance.visits_sum(body)
  p result
rescue LinksClient::ApiError => e
  puts "Exception when calling LinksApi->visits_sum: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LinksVisitsSumRequest**](LinksVisitsSumRequest.md)|  | 

### Return type

[**LinksVisitsSumResponse**](LinksVisitsSumResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



