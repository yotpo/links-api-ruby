# LinksClient::LinksStatsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stats** | [**Array&lt;LinksStatsRow&gt;**](LinksStatsRow.md) |  | [optional] 


