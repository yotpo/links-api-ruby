# LinksClient::LinksVisitsSumResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visits_sum** | **Integer** |  | [optional] 


