# LinksClient::LinksLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_key** | **String** |  | [optional] 
**destination_app_key** | **String** |  | [optional] 
**destination_domain_key** | **String** |  | [optional] 
**reference_code** | **String** |  | [optional] 
**source_domain_key** | **String** |  | [optional] 
**sub_reference** | **String** |  | [optional] 
**url** | **String** |  | [optional] 


