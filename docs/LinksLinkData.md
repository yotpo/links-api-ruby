# LinksClient::LinksLinkData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**LinksLink**](LinksLink.md) |  | [optional] 


