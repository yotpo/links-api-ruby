# LinksClient::LinksVisitsSumRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_key** | **String** |  | [optional] 
**in_field** | **String** |  | [optional] 
**in_values** | **Array&lt;String&gt;** |  | [optional] 
**url** | **String** |  | [optional] 
**url_regex** | **String** |  | [optional] 


