# LinksClient::LinksStatsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appkeys** | **Array&lt;String&gt;** |  | [optional] 
**count** | **Integer** |  | [optional] 
**date_interval** | **String** |  | [optional] 
**internal_ref_codes** | **Array&lt;String&gt;** |  | [optional] 


