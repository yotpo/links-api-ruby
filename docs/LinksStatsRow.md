# LinksClient::LinksStatsRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_key** | **String** |  | [optional] 
**count** | **Integer** |  | [optional] 
**day** | **Integer** |  | [optional] 
**id** | **String** |  | [optional] 
**month** | **Integer** |  | [optional] 
**reference** | **String** |  | [optional] 
**week** | **Integer** |  | [optional] 
**year** | **Integer** |  | [optional] 


