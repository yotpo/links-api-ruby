# LinksClient::LinksStatsByDateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appkeys** | **Array&lt;String&gt;** |  | [optional] 
**end_date** | **String** |  | [optional] 
**group_by** | **Array&lt;String&gt;** |  | [optional] 
**start_date** | **String** |  | [optional] 


