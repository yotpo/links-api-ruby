## go-links api client gem

### Install

    gem 'links-client', :git => 'https://gitlab.com/yotpo/links-api-ruby.git'

### Configure
```ruby
LinksClient.configure do |c|
  c.host = 'localhost:1234'
  c.base_path = 'v2'
  c.debugging = true
  c.timeout = 2
end
```

### Use
```ruby
require 'links-client'

links_client = LinksClient::LinksApi.new
begin
  result = links_client.create({ data: { app_key: app_key, url: url, reference_code: reference.code }})
rescue LinksClient::ApiError => e
  puts "Exception when calling LinksApi->create: #{e}"
end

```

### Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*LinksClient::LinksApi* | [**create**](docs/LinksApi.md#create) | **POST** /v1 | 
*LinksClient::LinksApi* | [**show**](docs/LinksApi.md#show) | **GET** /v1/{code} | 
*LinksClient::LinksApi* | [**stats**](docs/LinksApi.md#stats) | **POST** /v1/stats | 
*LinksClient::LinksApi* | [**stats_by_date**](docs/LinksApi.md#stats_by_date) | **POST** /v1/stats_by_date | 
*LinksClient::LinksApi* | [**visits_sum**](docs/LinksApi.md#visits_sum) | **POST** /v1/visits_sum | 


### Documentation for Models

 - [LinksClient::LinksCreateResponse](docs/LinksCreateResponse.md)
 - [LinksClient::LinksLink](docs/LinksLink.md)
 - [LinksClient::LinksLinkData](docs/LinksLinkData.md)
 - [LinksClient::LinksShowRequest](docs/LinksShowRequest.md)
 - [LinksClient::LinksStatsByDateRequest](docs/LinksStatsByDateRequest.md)
 - [LinksClient::LinksStatsRequest](docs/LinksStatsRequest.md)
 - [LinksClient::LinksStatsResponse](docs/LinksStatsResponse.md)
 - [LinksClient::LinksStatsRow](docs/LinksStatsRow.md)
 - [LinksClient::LinksVisitsSumRequest](docs/LinksVisitsSumRequest.md)
 - [LinksClient::LinksVisitsSumResponse](docs/LinksVisitsSumResponse.md)

