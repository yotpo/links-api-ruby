=begin
#links.proto

#No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

OpenAPI spec version: version not set

Generated by: https://github.com/swagger-api/swagger-codegen.git

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=end

require "uri"

module LinksClient
  class LinksApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [LinksCreateResponse]
    def create(body, opts = {})
      data, _status_code, _headers = create_with_http_info(body, opts)
      return data
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(LinksCreateResponse, Fixnum, Hash)>] LinksCreateResponse data, response status code and response headers
    def create_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: LinksApi.create ..."
      end
      # verify the required parameter 'body' is set
      fail ArgumentError, "Missing the required parameter 'body' when calling LinksApi.create" if body.nil?
      # resource path
      local_var_path = "/v1".sub('{format}','json')

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LinksCreateResponse')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LinksApi#create\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # 
    # 
    # @param code 
    # @param [Hash] opts the optional parameters
    # @return [LinksLinkData]
    def show(code, opts = {})
      data, _status_code, _headers = show_with_http_info(code, opts)
      return data
    end

    # 
    # 
    # @param code 
    # @param [Hash] opts the optional parameters
    # @return [Array<(LinksLinkData, Fixnum, Hash)>] LinksLinkData data, response status code and response headers
    def show_with_http_info(code, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: LinksApi.show ..."
      end
      # verify the required parameter 'code' is set
      fail ArgumentError, "Missing the required parameter 'code' when calling LinksApi.show" if code.nil?
      # resource path
      local_var_path = "/v1/{code}".sub('{format}','json').sub('{' + 'code' + '}', code.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LinksLinkData')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LinksApi#show\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [LinksStatsResponse]
    def stats(body, opts = {})
      data, _status_code, _headers = stats_with_http_info(body, opts)
      return data
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(LinksStatsResponse, Fixnum, Hash)>] LinksStatsResponse data, response status code and response headers
    def stats_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: LinksApi.stats ..."
      end
      # verify the required parameter 'body' is set
      fail ArgumentError, "Missing the required parameter 'body' when calling LinksApi.stats" if body.nil?
      # resource path
      local_var_path = "/v1/stats".sub('{format}','json')

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LinksStatsResponse')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LinksApi#stats\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [LinksStatsResponse]
    def stats_by_date(body, opts = {})
      data, _status_code, _headers = stats_by_date_with_http_info(body, opts)
      return data
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(LinksStatsResponse, Fixnum, Hash)>] LinksStatsResponse data, response status code and response headers
    def stats_by_date_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: LinksApi.stats_by_date ..."
      end
      # verify the required parameter 'body' is set
      fail ArgumentError, "Missing the required parameter 'body' when calling LinksApi.stats_by_date" if body.nil?
      # resource path
      local_var_path = "/v1/stats_by_date".sub('{format}','json')

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LinksStatsResponse')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LinksApi#stats_by_date\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [LinksVisitsSumResponse]
    def visits_sum(body, opts = {})
      data, _status_code, _headers = visits_sum_with_http_info(body, opts)
      return data
    end

    # 
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(LinksVisitsSumResponse, Fixnum, Hash)>] LinksVisitsSumResponse data, response status code and response headers
    def visits_sum_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: LinksApi.visits_sum ..."
      end
      # verify the required parameter 'body' is set
      fail ArgumentError, "Missing the required parameter 'body' when calling LinksApi.visits_sum" if body.nil?
      # resource path
      local_var_path = "/v1/visits_sum".sub('{format}','json')

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'LinksVisitsSumResponse')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LinksApi#visits_sum\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
